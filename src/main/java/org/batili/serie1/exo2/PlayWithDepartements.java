package org.batili.serie1.exo2;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;



public class PlayWithDepartements {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
//Map<String,Departement>Departements=new HashMap<>();
		Connection connection = 
				DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/tp_jdbc", 
						"tp-jdbc-user", "user");
		String sql = "insert into Departement(id,nom) values (?,?)";
		PreparedStatement preparedStatement=connection.prepareStatement(sql);
		Path path=Path.of("data/departement.csv");
		BufferedReader reader;
		try {
			reader = Files.newBufferedReader(path);
			String line=reader.readLine();
		line=reader.readLine();
		while (line != null) {
			String[] split=line.split(";");
			String id=split[0];
	String	nom=split[1];
	Departement departement=new Departement(id, nom);
	preparedStatement.setString(1, id);
	preparedStatement.setString(2, nom); 
	preparedStatement.addBatch();
	line=reader.readLine();
	

		} 
		int[] counts= preparedStatement.executeBatch();
		int count=Arrays.stream(counts).sum();
		System.out.println("nombre de communes cr�es :"+count);
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		


	

}
}
