package org.batili.serie1.exo2;

public class Departement {
	private String id;
	private String nom;
	public Departement(String id,String nom) {
		// TODO Auto-generated constructor stub
		this.id=id;
		this.nom=nom;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Departement [id=" + id + ", nom=" + nom + "]";
	}

}
