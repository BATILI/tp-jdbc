package org.batili.serie1.exo1;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.batili.serie1.exo1.model.Commune;

public class AnalyseCSV {

	public static void main(String[] args) throws SQLException {
		
		// TODO Auto-generated method stub
		Map<String,Commune> communes=new HashMap<>();
		Connection connection = 
				DriverManager.getConnection(
						"jdbc:mysql://localhost:3306/tp_jdbc", 
						"tp-jdbc-user", "user");
		String sql = "insert into commune(codepostal,nom) values (?,?)";
		PreparedStatement preparedStatement=connection.prepareStatement(sql);
		String sql0=" insert into Maire (first_name,last_name,civility,date_of_birt,code_insee) values (?,?,?,?,?)";
		PreparedStatement psmnt0 = connection.prepareStatement(sql0);
		
		String sql1=" insert into Departement (departement_name) values (?)";
		PreparedStatement psmnt = connection.prepareStatement(sql1);
		
		Path path=Path.of("data/maires-25-04-2014.csv");
		try {
			BufferedReader reader= Files.newBufferedReader(path);
			String line=reader.readLine();
			line=reader.readLine();
			while (line != null) {
				String[] split=line.split(";");
				String CodeDepartement=split[0];
				if (CodeDepartement.length()==1) {
					CodeDepartement="0"+ CodeDepartement;
					
				}
				String CodeInsee=split[2];


				if (CodeInsee.length()==1) {
					CodeInsee="00"+CodeInsee;
					
				} else if(CodeInsee.length()==2) {
					CodeInsee="0"+CodeInsee;
				}
				
				String CodePostal=CodeDepartement+CodeInsee;
				String nom=split[3];
				Commune commune=new Commune(nom,CodePostal);
				Commune previousCommune=communes.put(CodePostal, commune);
				if (previousCommune!=null) {
					
					System.out.println("Doublons="+previousCommune);
				}else {
					preparedStatement.setString(1, CodePostal);
					preparedStatement.setString(2, nom); 
					preparedStatement.addBatch();

				}
				
				
				

				line=reader.readLine();
				
				
			}
			int[] counts= preparedStatement.executeBatch();
			int count=Arrays.stream(counts).sum();
			System.out.println("nombre de communes cr�es :"+count);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}

}
