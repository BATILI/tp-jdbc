package org.batili.serie1.exo3;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;




public class Main {


	public static void main(String[] args) throws SQLException, IOException, ParseException {

		Connection connection= DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/tp_jdbc", 
				"tp-jdbc-user", "user");  

		

		String sql0=" insert into Maire (first_name,last_name,civility,date_of_birt,code_insee) values (?,?,?,?,?)";
		String sql=" insert into Departement (departement_name) values (?)";
		String sql1=" insert into Commune (commune_name, id_maire,departement_name) values (?,?,?)";

		PreparedStatement psmnt = connection.prepareStatement(sql);
		PreparedStatement psmnt0 = connection.prepareStatement(sql0);
		PreparedStatement psmnt1 = connection.prepareStatement(sql1);

		


		String file="data/maires-25-04-2014.csv";
		Reader reader = new FileReader(file);

		@SuppressWarnings("resource")
		BufferedReader br= new BufferedReader(reader);


		String line = br.readLine();
		line = br.readLine();


		String[] tableauData;

		while(line!=null) {
			tableauData=line.split(";");


			String departement_code= tableauData[0];
			String departement_name= tableauData[1];
			String code_insee = tableauData[2];
			String commune_name= tableauData[3];
			String population_commune= tableauData[4];
			String last_name = tableauData[5];
			String first_name = tableauData[6];
			String civility = tableauData[7];
			String date_of_birt = tableauData[8];
			String profession_code= tableauData[9];
			String profession_name= tableauData[10];


			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yy");
			Date date1 = (Date) sdf.parse(date_of_birt);	



			java.sql.Date date_sql = new java.sql.Date(date1.getTime());

			psmnt.setString(1,departement_code);
			psmnt.setString(2,departement_name);
			psmnt.setString(3,code_insee);
			psmnt.setString(4,commune_name);
			psmnt.setString(5,population_commune);
			psmnt.setString(6,last_name);
			psmnt.setString(7,first_name);
			psmnt.setString(8,civility);
			psmnt.setDate(9,date_sql);
			psmnt.setString(10,profession_code);
			psmnt.setString(11,profession_name);

			psmnt.addBatch();
			line = br.readLine();
		}


		//psmnt.executeBatch();
		countMaire();
		countMaire("M");                       // Civility 
		getOldestMaire();
		countDepartementPopulation("AIN");        // departement_name 
		countPopulation();
		countMaire(1955);                      // year(date_of_birth)
		countCivilityByAge("Mme",50);             // Civility, age
	}



//***********************************************************************************************************

	public static void countMaire() throws SQLException {
		Connection connection= DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/tp_jdbc", 
				"tp-jdbc-user", "user"); 

		String sql=" select count(distinct first_name, last_name) from Maire ";
		PreparedStatement psmnt = connection.prepareStatement(sql);

		ResultSet rs =psmnt.executeQuery();
		if (rs.next()) {

			System.out.println("nombre de maires est: " + rs.getInt(1));
		}
	}

//***********************************************************************************************************

	public static void countMaire(String Civilite) throws SQLException {
		Connection connection= DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/tp_jdbc", 
				"tp-jdbc-user", "user"); 

		String sql=" select count(distinct first_name , last_name) from Maire where civility='" + Civilite + "'";
		PreparedStatement psmnt = connection.prepareStatement(sql);

		ResultSet rs =psmnt.executeQuery();
		if (rs.next()) {

			System.out.println("nombre de maires de civilit�: " + Civilite + " est: " + rs.getInt(1));
		}
	}
//***********************************************************************************************************

	public static void getOldestMaire() throws SQLException {
		Connection connection= DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/tp_jdbc", 
				"tp-jdbc-user", "user"); 

		String sql=" select last_name, first_name, min(date_of_birt) from Maire;  ";
		PreparedStatement psmnt = connection.prepareStatement(sql);

		ResultSet rs =psmnt.executeQuery();
		if (rs.next()) {

			System.out.println("le maire le plus ag� est: " + rs.getString(1) + " " + rs.getString(2));
		}

	}
//***********************************************************************************************************

	public static void countDepartementPopulation(String Departement) throws SQLException {
		Connection connection= DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/tp_jdbc", 
				"tp-jdbc-user", "user"); 

		String sql=" select sum(population_commune) from Maire where departement_name='" + Departement +"'";
		PreparedStatement psmnt = connection.prepareStatement(sql);

		ResultSet rs =psmnt.executeQuery();
		if (rs.next()) {

			System.out.println("la population du departement: " + Departement + " est: " + rs.getInt(1));
		}

	}
//***********************************************************************************************************

	public static void countPopulation() throws SQLException {
		Connection connection= DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/tp_jdbc", 
				"tp-jdbc-user", "user"); 

		String sql=" select sum(population_commune) from Maire ";
		PreparedStatement psmnt = connection.prepareStatement(sql);

		ResultSet rs =psmnt.executeQuery();
		if (rs.next()) {

			System.out.println("la population totale de toutes les communes est: " + rs.getInt(1));
		}

	}
//***********************************************************************************************************

	public static void countMaire(int year) throws SQLException {
		Connection connection= DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/tp_jdbc", 
				"tp-jdbc-user", "user"); 

		String sql=" select count(distinct first_name, last_name) from Maire where Year(date_of_birt)='" + year + "'";
		PreparedStatement psmnt = connection.prepareStatement(sql);

		ResultSet rs =psmnt.executeQuery();
		if (rs.next()) {

			System.out.println("le nombre de maires ayant le m�me age est: " + rs.getInt(1));
		}

	}
//***********************************************************************************************************

	public static void countCivilityByAge(String Civilite,int age) throws SQLException {
		Connection connection= DriverManager.getConnection(
				"jdbc:mysql://localhost:3306/tp_jdbc", 
				"tp-jdbc-user", "user"); 

		String sql=" select count(distinct first_name , last_name) from Maire where (TIMESTAMPDIFF(YEAR, date_of_birt, CURDATE()))='" + age +"' and Civility='" + Civilite + "'";
		PreparedStatement psmnt = connection.prepareStatement(sql);

		ResultSet rs =psmnt.executeQuery();
		if (rs.next()) {

			System.out.println("le nombre de maires de civilit�: " + Civilite + " ayant le m�me �ge est: " + rs.getInt(1));
		}

	}

}

